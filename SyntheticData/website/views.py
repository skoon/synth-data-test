from django.shortcuts import render, HttpResponse
import json
from .SyntheticApp import Dummy_Fields
from .SyntheticApp import generate_data
from faker import Faker
import inspect
import csv
import time

# Data Type List
a = Dummy_Fields(Faker('en_GB'))
attr_list = list(a.__dict__.keys())
for name, value in inspect.getmembers(Dummy_Fields, predicate=inspect.isfunction):
    if name != "__init__":
        attr_list.append(name)

# Create your views here.
def home(request):
    var = {'node': 'yes'}
    
    if request.method == "POST":    # Submit Synthetic dataset request

        # Converting Data into list arguments
        column_name_array = request.POST.getlist("column_name")
        data_type_array = [w.replace(' ', '_') for w in request.POST.getlist('data_type')]
        num_of_entries = int(request.POST['num_of_entries'])
        format_list = request.POST.getlist("format_option")
        blank_perc_list = [int(w) for w in request.POST.getlist('blankPercent')]
        bad_data_list = request.POST.getlist("badData")

        print(data_type_array)
        
        # Generate Data
        task_progress = generate_data(num_of_entries,column_name_array,data_type_array,format_list,blank_perc_list,bad_data_list)

        # Send Data to dowload on front end
        with open('SyntheticData.csv') as csvfile:
            response = HttpResponse(csvfile,content_type='document')
            response['Content-Disposition'] = 'attachment; filename="SyntheticData.csv"'
            return response
        render(request,'',{"my_data_2":var})

    else:   # Home page - get request

        # Data Type List
        with open('static/website/js/attr_list.js', 'w') as out_file:
            out_file.write('var attr_list = %s;' % json.dumps(attr_list))

        return render(request,'home.html',{"my_data":var})


##progress bar test
# from time import time as t
# val_list = []
# nowTime = t()
# for i in range(100):
#     val = i*2
#     print(val)
#     curTime = t()
#     if curTime - nowTime >= 0.1:
#         #Do your stuff
#         print("iterator",i)
#         nowTime = curTime

def about(request):
    return render(request, "about.html",{})
