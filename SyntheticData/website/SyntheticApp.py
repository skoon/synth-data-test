import csv
import itertools
import os
import datetime
from datetime import date
import random
from random import randint
import string
# import exrex
import time
from faker import Faker
from barnum import gen_data
import random
from optparse import OptionParser
import inspect
from itertools import count
## Define functions for random generation of data

def csv_array_reader(csv_file):
    """    Read in a single column csv as an array    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=",")
        for row in reader:
            list_csv.append(row[0])
    return list_csv

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def POST_CODE(self):
    return self.address[-1]

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def MOBILE(fake_var):
    mobile = fake_var.cellphone_number()
    mobile = ''.join(char for char in mobile if char.isalnum())
    return mobile

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

# def get_industries():
#     """
#     read in all industries and SIC codes from csv to dict
#     """
#     CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
#     Industry_dict = csv_dict_reader(CSV_FILE)
#     return Industry_dict

########################################################################################################################

id_count = 1

class Dummy_Fields():

    _ids=count(0)

    def __init__(self,fake_var):

        #fake_var = Faker('en_GB')
        #UK_COUNTIES = get_UK_Counties()

        #self.name = fake_name(fake_var)
        #self.first_name = fake_name(fake_var)[1]
        #self.name = fake_name(fake_var)[1] + ' ' + fake_name(fake_var)[2]
        #self.phone = MOBILE(fake_var)
        #self.postcode = POST_CODE(fake_var)
        #self.gender = random.choice(["M","F","N/A"])
        # self.account_type = random.choice(["ISA","Pension","Savings","Investment"])
        #self.percent = randomNumberString(2) + "%"
        
        # self.Fake_Id = str(id_count) 
        self.Id = ""    #str(next(self._ids))

    def Multiple_Choice(self,fake_var,string):
        return random.choice(string.split(","))

    def String(self,fake_var,format):
        """        Join letters together for a random string of defined length        """
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(int(format)))

    def Number(self,fake_var,format):
        """        Join letters together for a random string of numbers of defined length        """
        # numbers = '0123456789'
        # ''.join(random.choice(numbers) for i in range(stringLength))
        min = int(format.split(",")[0])
        max = int(format.split(",")[1])
        return randint(min,max)

    def Email(self,fake_var):
        return fake_var.email()

    def Address(self,fake_var,format):
        """     Prerequists:Instantiate Faker for UK addresses and names - Read in file of UK counties
                Run: Gather address from faker - split it - Add a county - Return it        """

        address = fake_var.address()
        address_array = address.split('\n')
        county = random_UK_Counties(get_UK_Counties())
        address_array.insert(-1, county)
        address = " ".join(address_array)

        if   format=="fullAddress": pass
        elif format=="addressLine1": address = address_array[1]
        elif format=="addressLine2": address = address_array[2]
        elif format=="county": address = county
        elif format=="postcode": address = address_array[-1]
        return address
        

    def Date(self, fake_var, format):
        """
                    Get a time at a proportion of a range of two formatted times.
                    start and end should be strings specifying times formated in the
                    given format (strftime-style), giving an interval [start, end].
                    The returned time will be in the specified format.
        """
        # date_format = "%d/%m/%y,1900-01-01,2000-01-01"
        # fake_var = Faker("en_GB")
        date_format = format.split(",")[0]
        # print(format)

        start = datetime.datetime.strptime(format.split(",")[1],'%Y-%m-%d')
        end = datetime.datetime.strptime(format.split(",")[2],'%Y-%m-%d')

        date = fake_var.date_time_between( start_date = start,end_date = end,tzinfo=None)
        # print(date)
        date = datetime.datetime.strftime(date, date_format)
        return date

    def Name(self,fake_var,format):
        if format == "fullName": return  fake_var.name()
        if format == "firstName": return  fake_var.first_name()
        if format == "lastName": return  fake_var.last_name()

    def Job(self,fake_var):
        return fake_var.job()

    def Text(self,fake_var,format):
        words = fake_var.text()
        if format == "": return words
        else: return ' '.join(words.split()[:int(format)])

########################################################################################################################

# class Generate_Data():
def generate_data(num_of_entries,column_name_array,data_type_array,format_list,blank_perc_list,bad_data_list):
    fake_var = Faker('en_GB')

    #open csv to write to
    with open('SyntheticData.csv', 'w', newline='') as csvfile:
        combination_writer = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for i in range(num_of_entries+1):   #iterate over rows
            if i == 0:  #column name row
                combination_writer.writerow(column_name_array)
                print(column_name_array)
            else:   #data rows                 
                dummy_fields = Dummy_Fields(fake_var)
                SETL_info =[]
                num_of_m_choice = 0

                # iterate over every data type selected
                for col in range(len(data_type_array)):
                    attr = data_type_array[col]
                    # Conditionals for 1. Data that has formatting 2. Id type  3.Other data types (including attributes as oppossed to methods)
                    if attr == "Multiple_Choice" or attr == "Date" or attr == "Address" or attr == "Name" or attr == "Number" or attr == "String" or attr == "Text":
                        method = getattr(dummy_fields,attr)
                        value = method(fake_var,format_list[num_of_m_choice])
                        num_of_m_choice+=1
                    elif attr == "Id":
                        value = i
                    else:
                        method = getattr(dummy_fields,attr)
                        if isinstance(method,str):
                            value = method
                        else:
                            value= method(fake_var)

                    # Bad Data
                    bad_data_level = bad_data_list[col]
                    if (attr == "Id" or attr == "Number") or bad_data_level == "0": pass
                    elif bad_data_level == "1": 
                        bad_char = [""," "]
                        value = random.choice(bad_char) + random.choice(bad_char) + value + random.choice(bad_char) + random.choice(bad_char)
                    elif bad_data_level == "2": 
                        bad_char = [""," ",",","."]
                        value = random.choice(bad_char) + random.choice(bad_char) + value + random.choice(bad_char) + random.choice(bad_char)
                    elif bad_data_level == "3": 
                        bad_char = [""," ",",","."";",":","@"]
                        value = random.choice(bad_char) + random.choice(bad_char) + value + random.choice(bad_char) + random.choice(bad_char)
                    elif bad_data_level == "4": 
                        bad_char = [""," ",",","."";",":","@",chr(10),chr(160)]
                        # value = random.choice(bad_char) + random.choice(bad_char) + value + random.choice(bad_char) + random.choice(bad_char)
                        value = ''.join('%s%s' % (x, random.choice(bad_char) if random.random() > 0.5 else '') for x in value)
                        

                    # Blank Values
                    blank_perc = blank_perc_list[col]
                    blank_perc = max(min(100, blank_perc), 0)   #restrict percent 0-100
                    value = random.choices([value,""],[100-blank_perc,blank_perc],k=1)[0]



                    SETL_info.append(value)

                combination_writer.writerow(SETL_info)
                print(SETL_info)
            # return i
    # return csvfile

# generate_data(5,[123,1],["Id"],m_choice_list=[])