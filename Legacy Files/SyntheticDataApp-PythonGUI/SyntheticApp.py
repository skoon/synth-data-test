import csv
import itertools
import os
import datetime
from datetime import date
import random
from random import randint
import string
import exrex
import time
from faker import Faker
from barnum import gen_data
import random
from optparse import OptionParser
import inspect
from itertools import count
## Define functions for random generation of data

def csv_array_reader(csv_file):
    """    Read in a single column csv as an array    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            list_csv.append(row[0])
    return list_csv

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def POST_CODE(self):
    return self.address[-1]

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def PERSONAL_EMAIL_ADRESS( fake_var):
    return fake_var.email()

def BUSINESS_EMAIL_ADDRESS(self, fake_var):
    return fake_var.email()

def MOBILE(fake_var):
    mobile = fake_var.cellphone_number()
    mobile = ''.join(char for char in mobile if char.isalnum())
    return mobile

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

# def get_industries():
#     """
#     read in all industries and SIC codes from csv to dict
#     """
#     CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
#     Industry_dict = csv_dict_reader(CSV_FILE)
#     return Industry_dict


########################################################################################################################

id_count = 0

class Dummy_Fields():

    _ids=count(0)

    def __init__(self,fake_var):

        #fake_var = Faker('en_GB')
        #UK_COUNTIES = get_UK_Counties()

        #self.name = fake_name(fake_var)
        #self.first_name = fake_name(fake_var)[1]
        #self.name = fake_name(fake_var)[1] + ' ' + fake_name(fake_var)[2]
        #self.phone = MOBILE(fake_var)
        #self.postcode = POST_CODE(fake_var)
        #self.gender = random.choice(["M","F","N/A"])
        self.account_type = random.choice(["ISA","Pension","Savings","Investment"])
        #self.percent = randomNumberString(2) + "%"
        self.id = str(next(self._ids))

    def multiple_choice(self,string):
        return random.choice(string.split(","))

    def string(self,fake_var,stringLength=6):
        """        Join letters together for a random string of defined length        """
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(stringLength))

    def number(self,fake_var,stringLength=6):
        """        Join letters together for a random string of numbers of defined length        """
        numbers = '0123456789'
        return ''.join(random.choice(numbers) for i in range(stringLength))

    def email(self,fake_var):
        return fake_var.email()

    def address(self,fake_var):
        """     Prerequists:Instantiate Faker for UK addresses and names - Read in file of UK counties
                Run: Gather address from faker - split it - Add a county - Return it        """
        address = fake_var.address()
        address = address.split('\n')
        county = random_UK_Counties(get_UK_Counties())
        address.insert(-1, county)
        address = " ".join(address)
        return address

    def date(self, fake_var, start='01/01/1950', end='01/01/2020', date_format='%d/%m/%Y'):
        """
                    Get a time at a proportion of a range of two formatted times.
                    start and end should be strings specifying times formated in the
                    given format (strftime-style), giving an interval [start, end].
                    The returned time will be in the specified format.
        """
        date = fake_var.date_time_between(start_date=datetime.datetime.strptime(start, date_format),
                                          end_date=datetime.datetime.strptime(end, date_format),
                                          tzinfo=None)
        date = datetime.datetime.strftime(date, date_format)
        return date

    def name(self,fake_var):
        return fake_var.name()

    def first_name(self,fake_var):
        return fake_var.first_name()
    def last_name(self,fake_var):
        return fake_var.last_name()

    def job(self,fake_var):
        return fake_var.job()

    def text(self,fake_var):
        return fake_var.text()

########################################################################################################################

# class Generate_Data():
def generate_data(num_of_entries,column_names_array,data_type_array,m_choice_list=[]):

    fake_var = Faker('en_GB')

    with open('SyntheticData.csv', 'w', newline='') as csvfile:
        combination_wrtier = csv.writer(csvfile, delimiter=',',
                                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for i in range(num_of_entries+1):
            if i == 0:
                combination_wrtier.writerow(column_names_array)

                print(column_names_array)
            else:
                dummy_fields = Dummy_Fields(fake_var)

                SETL_info =[]
                num_of_m_choice = 0
                for attr in data_type_array:
                    if attr == "multiple_choice":
                        method = getattr(dummy_fields,attr)

                        SETL_info.append(method(m_choice_list[num_of_m_choice]))
                        num_of_m_choice+=1
                    else:
                        method = getattr(dummy_fields,attr)

                        if isinstance(method,str):
                            value = method
                        else:
                            value= method(fake_var)

                        SETL_info.append(value)
                        #SETL_info.append(getattr(dummy_fields, attr))

                # SETL_info = [
                #      'A-' + str(i),
                #     dummy_fields.random_number,
                #     dummy_fields.email,
                #              ]

                combination_wrtier.writerow(SETL_info)

                print(SETL_info)

#generate_data(10,["name","dubndf","isdfinkf","ibdfk","gdg","gd"],["random_number","name"])

#column_names = ['Advisor_ID', 'FCA_Number', 'Email', 'Name', 'Phone_Number', 'Type']

#generate_data(3,['Advisor_ID', 'FCA_Number', 'Email', 'Name', 'Phone_Number', 'Type'])

# inspect.getmembers(OptionParser, predicate=inspect.ismethod)
#print(inspect.getmembers(Dummy_Fields, predicate=inspect.isfunction))


# fake_var = Faker('en_GB')
# a=Dummy_Fields(fake_var=fake_var)
#
# # print(dir(a))
#
# method_list = [func for func in dir(a) if callable(getattr(a, func)) and not func.startswith("_")]
# print(method_list)
# fake_var = Faker('en_GB')
# print(fake_var.name())
#print(fake_var.name


