from tkinter import *
from faker import Faker
from tkinter import ttk, messagebox
from ttkthemes import themed_tk as tk
#from SyntheticDataAppv2 import SyntheticApp
from SyntheticApp import Dummy_Fields
from SyntheticApp import generate_data
import pprint
import inspect

def rgb_color(rgb):
    return '#%02x%02x%02x' % rgb

root = Tk()

#---Styling---#
#root.geometry("")
root.geometry("600x400")
root.title("Synthetic Data")
bg_colour = "gray14"
fg_colour = "white"
button_colour = rgb_color((55, 90, 127))
root.configure(bg=bg_colour)
# combostyle = ttk.Style()
#
# combostyle.theme_create('combostyle',
#                          settings = {'TCombobox':
#                                      {'configure':
#                                       {'font': 'calibri 13 bold'
#
#                                        #'fieldbackground': 'red'
#                                        }}}
#                          )
# # ATTENTION: this applies the new style 'combostyle' to all ttk.Combobox
# combostyle.theme_use('combostyle')


#---Frames---#
headingFrame = Frame(root,bg=bg_colour)
headingFrame.grid(row=0)

topFrame = Frame(root,bg=bg_colour)
topFrame.grid(row=2)

bottomFrame = Frame(root,bg=bg_colour)
bottomFrame.grid(row=3)

#---Heading---#
headingLabel = Label(headingFrame,text="Synthetic Data",font='Gotham 20 bold', bg=bg_colour,fg=fg_colour)
headingLabel.pack(padx=200,pady=20)

#---Number of Data Entries---#
dataNumLabel = Label(bottomFrame,text="No. of Rows:",bg=bg_colour,fg=fg_colour)
dataNumLabel.grid(row=0,column=1)
dataNumEntry = Entry(bottomFrame)
dataNumEntry.grid(row=0,column=2)

#---Columns Names---#
# colNameFrame = Frame(topFrame)
# colNameFrame.grid(row = 1)
fieldLabel = Label(topFrame, text="Column Name",font='Gotham 12 bold',bg=bg_colour,fg=fg_colour)
fieldLabel.grid(row=1, column=1)
dataTypeLabel = Label(topFrame, text="Data Type",font='Gotham 12 bold',bg=bg_colour,fg=fg_colour)
dataTypeLabel.grid(row=1, column=2)

#---List of Widget to Track---#
entry_count=[]
combobox_count = []
#m_choice_count=[]
m_choice_count={}

#---DataType Menu---#

a = Dummy_Fields(Faker('en_GB'))
# attr_list = [func for func in dir(a) if  not func.startswith("_")]  #experiment
attr_list = list(a.__dict__.keys())

for name, value in inspect.getmembers(Dummy_Fields, predicate=inspect.isfunction):
    if name != "__init__":
        attr_list.append(name)

print(attr_list)

#---Add Another Field---#
def addField():
    next_row = len(entry_count) + 2

    ent = Entry(topFrame)
    ent.grid(row=next_row,column=1,padx=10)

    def m_choice(event):
        if cbox.get() == "multiple_choice":
            m_choice_ent = Entry(topFrame)
            m_choice_ent.grid(row=next_row,column = 3,padx=10)
            #m_choice_count.append(m_choice_ent)
            m_choice_count[next_row] = m_choice_ent

    cbox = ttk.Combobox(topFrame, value=attr_list)
    cbox.bind("<<ComboboxSelected>>", m_choice)
    cbox.grid(row=next_row, column=2)

    def delete_button_func():
        combobox_count.remove(cbox)
        entry_count.remove(ent)

        # delete all widgets on the row
        entryListRow = int(ent.grid_info()["row"])
        for widget in topFrame.grid_slaves():
            if int(widget.grid_info()["row"])== entryListRow:
                if int(widget.grid_info()["column"]) == 3:
                    #m_choice_count.remove(widget)
                    del m_choice_count[entryListRow]
                widget.grid_forget()

        #print(m_choice_count)
        # ent.grid_forget()
        # deleteButton.grid_forget()
        # cbox.grid_forget()
        # fieldFrame.destroy()

    deleteButton = Button(topFrame,text=" - ",command=delete_button_func,bg=button_colour,fg=fg_colour,font="calibri 11 bold")
    deleteButton.grid(row=next_row,column=0)

    combobox_count.append(cbox)
    entry_count.append(ent)


addField()

addButton = Button(topFrame, text=" + ",command=addField,bg=button_colour,fg=fg_colour,font="calibri 11 bold")
addButton.grid(row=100, column=0,sticky='s')

#---Final Generate Button---#
def generate_button():
    entryList = []
    m_choiceList = []
    cboxList = []

    #Column Names
    for ent in entry_count:
        entryList.append(ent.get())

    #Data Types
    for data in combobox_count:
        cboxList.append(data.get())

    #Populate M_choice options list if they exists
    for key in sorted(m_choice_count.keys()):
        m_choiceList.append(m_choice_count[key].get())

    # for choice in m_choice_count:
    #     m_choiceList.append(choice.get())

    loop_counter = int(dataNumEntry.get())

    generate_data(loop_counter,entryList,cboxList,m_choiceList)

    messagebox.showinfo(message="Your Data Has Been Generated!")

generateButton = Button(bottomFrame, text=" Generate Data ", command=generate_button,bg=button_colour,fg=fg_colour)
generateButton.grid(row=0, column=3,sticky='s')




root.mainloop()