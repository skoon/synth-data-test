# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 11:23:07 2020

@author: mmorganwilliams
"""
import csv
import itertools
import os
import datetime
import random
import string
import exrex
import time
from faker import Faker
from barnum import gen_data

def randomString(stringLength):
    """
    Join letters together for a random string of defined length
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))

def randomNumberString(stringLength):
    """
    Join letters together for a random string of numbers of defined length
    """
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(stringLength))

def random_date(fake_var, start, end, date_format='%d%m%Y'):
    """
    Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    The returned time will be in the specified format.
    """
    date = fake_var.date_time_between(start_date=datetime.datetime.strptime(start, date_format),
                                      end_date=datetime.datetime.strptime(end, date_format),
                                      tzinfo=None)
    date = datetime.datetime.strftime(date, date_format)

    return date

def randomMonthPeroid():
    """
    Select from random month periods between 3 and 48 months,
    Return a string
    """
    month_periods = ['3', '6', '12', '18', '24', '36', '48']
    return random.choice(month_periods)

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

def csv_array_reader(csv_file):
    """
    Read in a single column csv as an array
    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            list_csv.append(row[0])

    return list_csv


def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_industries():
    """
    read in all industries and SIC codes from csv to dict
    """
    CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
    Industry_dict = csv_dict_reader(CSV_FILE)
    return Industry_dict

def random_industry(Industry_dict):
    """
    Using dictionary keys (SIC) pick a random industry and return
    SIC and Industry
    """
    keys = list(Industry_dict.keys())
    random_ind = random.choice(keys)
    return [random_ind, Industry_dict[random_ind]]

def get_account_nos():
    """
    Get all account numbers from csv
    """
    CSV_FILE = os.path.join('Config', 'Account_Nos.csv')
    return csv_array_reader(CSV_FILE)

def random_account_no(all_account_nos):
    """
    Select an account numbers from array
    """
    return random.choice(all_account_nos)

def fake_address(fake_var, Counties):
    """
    Prerequists:
    Instantiate Faker for UK addresses and names
    Read in file of UK counties
    Run:
    Gather address from faker
    split it
    Add a county
    Return it
    """
    address = fake_var.address()
    address = address.split('\n')
    county = random_UK_Counties(Counties)
    address.insert(-1, county)
    return address

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    -
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def create_job_title():
    """
    Make a job title using Barnum
    """
    return gen_data.create_job_title()


class SETL_STMT_headers():
    """
    Class for all CHL headers
    """
    def __init__(self):
        """
        Setting headers for each file as attributes
        """
        SETL_STMT_csv_file = os.path.join('Config', 
                                      'SETL_STMT_LINES_headers.csv')
        self.setl_stmt_headers = csv_array_reader(SETL_STMT_csv_file)
        
class SETL_STMT_fields():
    def RECID(self):
        return randomNumberString(8)
    def TRANSACTION_CODE(self):
        return  random.choice(['297','993','998','878','898','90'])
    def ACCOUNT_NUMBER(self, account_nos):
        return random.choice(account_nos)
    def DATE_TIME(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    def AMOUNT_LCY(self):
        return randomNumberString(4)
    def AMOUNT_FCY(self):
        return randomNumberString(4)
    def KEY_ID(self):
        return randomNumberString(8)

    

class SETL_STMT():
    def __init__(self, account_nos, fake_var):
        self.recid = SETL_STMT_fields.RECID(self)
        self.transaction_code = SETL_STMT_fields.TRANSACTION_CODE(self)
        self.account_number = SETL_STMT_fields.ACCOUNT_NUMBER(self, account_nos)
        self.date_time = SETL_STMT_fields.DATE_TIME(self, fake_var)
        self.amount_lcy = SETL_STMT_fields.AMOUNT_LCY(self)
        self.amount_fcy = SETL_STMT_fields.AMOUNT_FCY(self)
        self.key_id = SETL_STMT_fields.KEY_ID(self)


        
ACCOUNT_NOS = get_account_nos()
FAKE = Faker('en_GB')
LOOP_COUNTER = 1000
with open('setl_stmt.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in range(LOOP_COUNTER):
        if i == 0:
            headers = SETL_STMT_headers()
            combination_wrtier.writerow(headers.setl_stmt_headers)
            
        else:
            SETL_customer = SETL_STMT(ACCOUNT_NOS, FAKE)
            SETL_info = [SETL_customer.recid,
                         '',
                         '',
                         '',
                        SETL_customer.transaction_code,
                        '',
                         '',
                         '',
                         '',
                        SETL_customer.account_number,
                        '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                        SETL_customer.date_time,
                        SETL_customer.amount_lcy,
                        SETL_customer.amount_fcy,
                        '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                        SETL_customer.key_id 
                        ]
            combination_wrtier.writerow(SETL_info)
