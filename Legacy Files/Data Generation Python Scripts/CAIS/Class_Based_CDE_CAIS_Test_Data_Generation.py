# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 12:57:18 2020

@author: mmorganwilliams
"""

import csv
import itertools
import os
import datetime
import random
import string
import exrex
import time
from faker import Faker
from barnum import gen_data
#https://pbpython.com/barnum.html
#https://github.com/chris1610/barnum-proj


class Company:
    def headers():
        header_identifier_header = "Header Identifier"
        source_code_header = "Source Code"
        creation_date_header = "Date of Creation"
        name_portfolio_header = "Company/Portfolio Name"
        filler_header1 = "Filler"
        CAIS_version_indicator_header = "CAIS Version Indicator"
        credit_card_bah_flag_header = "Credit Card Behavioural Sharing Flag"
        filler_header2 = "Filler"
        account_number_header = "Account Number" 
        person_number_header = "Person Number"
        business_type_indicator_header = "Business Type Indicator"
        name_address_header = "Name and Address"
        address_type_header = "Address Type"
        name_change_header = "Name Change"
        company_reg_number_header = "Company registration number"
        SIC_header = "SIC Code"
        VAT_no_header = "VAT Number"
        year_business_started_header = "Year Business Started"
        additional_trad_style_header = "Additional Trading Style"
        tel_no_header = "Telephone Number"
        business_web_address = "Business Website Address"
        poc_name_header = "Point of Contact Name"
        poc_email_header = "Point of Contact Email Address"
        poc_tel_header = "Point of Contact Telephone Number"
        poc_job_header = "Point of Contact Job Title"
        par_com_name_address_header = "Parent Company Name and Address"
        par_com_reg_no_header = "Parent Company Registered Number" 
        par_com_tel_no_header = "Parent Company Telephone Number"
        par_com_VAT_no_header = "Parent Company VAT Number"
        previous_name_address_header = "Previous name and address"
        experinan_expin_header = "Experian ExPin customer PIN"
        person_type_header = "Person Type Indicator"
        signatory_flag_header = "Signatory on the Account Flag"
        shareholders_flag_header = "Shareholders Flag"
        country_reg_header = "Country of Registration"
        DoB_header = "Date of Birth"
        prop_dir_guarantee_header = "Proprietors/Directors Guarantee"
        prop_dir_guarantee_can_header = "Proprietors/Directors Guarantee cancelled/discharged"
        filler_header3 = "Filler"
        account_type_header = "Account Type"
        start_date_header = "Start Date"
        close_date_header = "Close Date"
        payment_amount_header = "Payment Amount"
        repayment_period_header = "Repayment Period"
        current_bal_header = "Current Balance"
        credit_bal_credit_ind_header = "Credit Balance Credit Indicator"
        staus_code_header = "Status Code"
        spec_instruction_ind_header = "Special Instruction Indicator"
        credit_limit_header = "Credit Limit"
        flag_settings_header = "Flag Settings"
        debenture_header = "Debenture"
        mortgage_flag_header = "Mortgage Flags"
        airtimestatus_flag_header = "Airtime Status Flag"
        trans_to_collec_acc_flag_header = "Transferred to Collection Account Flag"
        balance_type_header = "Balance Type"
        credit_turnover_header = "Credit Turnover"
        primary_acc_indicator_header = "Primary Account Indicator"
        default_satisfaction_date_header = "Default Satisfaction Date"
        rejection_flag_header = "Rejection Flag"
        bank_sort_code_header = "Bank sort code"
        original_default_bal_header = "Original Default Balance"
        payment_freq_indicator_header = "Payment Frequency Indicator"
        no_credit_cards_header = "Number of Credit Cards issued"
        repayment_amount_header = "Repayment Amount"
        repayment_amount_pay_cred_ind_header = "Repayment Amount Payment Credit Indicator"
        prev_bal_header = "Previous Statement Balance"
        prev_bal_ind_header = "Previous Statement Balance Indicator"
        no_of_cash_advances_header = "Number of Cash Advances" 
        value_of_cash_advances_header = "Value of Cash Advances"
        payment_code_header = "Payment Code"
        promotion_activity_flag_header = "Promotion Activity Flag"
        payment_type_header = "Payment Type"
        new_acc_no_header = "New Account Number"
        new_person_no_header = "New Person Number"
        trailer_ID_header = "Trailer Identifier"
        total_records_header = "Total number of records"
        filler_header4 = "Filler"
        all_headers = [ header_identifier_header, 
               source_code_header,
               creation_date_header,
               name_portfolio_header,
               filler_header1,
               CAIS_version_indicator_header,
               credit_card_bah_flag_header,
               filler_header2,
               account_number_header,
               person_number_header,
               business_type_indicator_header,
               name_address_header,
               address_type_header,
               name_change_header,
               company_reg_number_header,
               SIC_header,
               VAT_no_header,
               year_business_started_header,
               additional_trad_style_header,
               tel_no_header,
               business_web_address,
               poc_name_header,
               poc_email_header,
               poc_tel_header,
               poc_job_header,
               par_com_name_address_header,
               par_com_reg_no_header,
               par_com_tel_no_header,
               par_com_VAT_no_header,
               previous_name_address_header,
               experinan_expin_header,
               person_type_header,
               signatory_flag_header,
               shareholders_flag_header,
               country_reg_header,
               DoB_header,
               prop_dir_guarantee_header,
               prop_dir_guarantee_can_header,
               filler_header3,
               account_type_header,
               start_date_header,
               close_date_header,
               payment_amount_header,
               repayment_period_header,
               current_bal_header,
               credit_bal_credit_ind_header,
               staus_code_header,
               spec_instruction_ind_header,
               credit_limit_header,
               flag_settings_header,
               debenture_header,
               mortgage_flag_header,
               airtimestatus_flag_header,
               trans_to_collec_acc_flag_header,
               balance_type_header,
               credit_turnover_header,
               primary_acc_indicator_header,
               default_satisfaction_date_header,
               rejection_flag_header,
               bank_sort_code_header,
               original_default_bal_header,
               payment_freq_indicator_header,
               no_credit_cards_header,
               repayment_amount_header,
               repayment_amount_pay_cred_ind_header,
               prev_bal_header,
               prev_bal_ind_header,
               no_of_cash_advances_header,
               value_of_cash_advances_header,
               payment_code_header,
               promotion_activity_flag_header,
               payment_type_header,
               new_acc_no_header,
               new_person_no_header,
               trailer_ID_header,
               total_records_header,
               filler_header4]

        return all_headers
        
        
    """
    Define functions for random generation of strings
    """
    def randomString(stringLength):
        """
        Join letters together for a random string of defined length
        """
        letters = string.ascii_letters
        return (''.join(random.choice(letters) for i in range(stringLength)))
    
    def randomNumberString(stringLength):
        """
        Join letters together for a random string of numbers of defined length
        """
        numbers = '0123456789'
        return (''.join(random.choice(numbers) for i in range(stringLength)))
    
    def random_date(start, end, date_format='%d%m%Y'):
        """
        Get a time at a proportion of a range of two formatted times.
    
        start and end should be strings specifying times formated in the
        given format (strftime-style), giving an interval [start, end].
        The returned time will be in the specified format.
        """
    
        stime = time.mktime(time.strptime(start, date_format))
        etime = time.mktime(time.strptime(end, date_format))
        ptime = stime + (random.random() * (etime - stime))
        random_date = time.strftime(date_format, time.localtime(ptime))
        
        return random_date
    
    def randomMonthPeroid():
        month_periods = ['3','6','12','18','24','36','48']
        return random.choice(month_periods)
            
    """
    Instantiate Faker for UK addresses and names
    """
    fake = Faker('en_GB')
    
    
    class Header():
        def Header():
            """
            # Header
            # Business Logic 
            # Must be 14 blanks followed by the word HEADER in capitals
            """
            return ("              HEADER")
        def source_code():
            """
            Source Code Hard coded to 12
            """
            source_code = "12"
            return (source_code)
        
        def creation_date():
            """
            Creation date is random between 2010 and 2030
            """
            creation_date = Company.random_date('01012010', '31122030')
            return(creation_date)
        
        def name_portfolio():
            """
            #Company/Portfolio name
            # First Line - Can the first line team confirm that they want to 
            # populate this field with 'MetroBank Plc'
            """
            name_portfolio = "MetroBank Plc"
            return name_portfolio
        
        def filler():
            """
            Filler
            """
            return ''
            
        def CAIS_version_indicator():
            """
            # CAIS Version Indicator
            # Hardcoded to CAISCOM2
            """
            CAIS_version_indicator = "CAISCOM2"
            return CAIS_version_indicator
        
        def credit_card_bah_flag():
            """
            # Credit Card Behavioural Sharing Flag
            # Hardcoded to Y
            """
            credit_card_bah_flag = 'Y'
            return credit_card_bah_flag
    
    class Account():
        def account_number():
            """
            # Account Number
            # Currently Generates a random 11 digit number
            """
            account_number =  Company.randomNumberString(11)
            return account_number
        
        def person_number():
            """
            # Person number
            # Generates a random 4 digit number
            """
            person_number = Company.randomNumberString(4)
            return person_number
        
        def business_type_indicator():
            """
            # Business Type Indicator
            # Random Choice between L and O
            """
            business_type_indicators = ['L','O']
            business_type_indicator = random.choice(business_type_indicators)
            return business_type_indicator
        
        def name_address():
            """
            # Name and Address
            # Generate a random company name
            # Prefix with CDE
            # Generate a random address
            # Insert Name into address and concatenate with ','
            # CONCATENATE the address fields
            """
            name = str(gen_data.create_company_name())
            name = 'CDE_' +''.join(name)
            address = Company.fake.address()
            address_split = address.split('\n')
            address_split.insert(0, name)
            name_address = ','.join(address_split)
            return name_address, name
        
        def address_type():
            """
            # Address Type
            # Hardcoded to O
            """
            address_type = "O"
            return address_type
        
        def name_change():
            """
            # Name Change
            # Random choice between Y and N for name change
            # Skew added against Y, it is not probable that 50% of companies have had a 
            # name change
            #IF PREVIOUS_NAME is not null 'Y' else 'N'
            """
            name_changes = ['Y','N','N','N','N']
            name_change = random.choice(name_changes)
            return name_change
        
        def company_reg_number():
            """
            # Company registration number
            # Generate a registration number based on the below patterns
            # England/Wales reg number = 8 digits
            # Scottish reg number = SC + 6 digits
            # England/Wales llp number = OC + 6 digits
            # Pick a country for the reg number
            """
            GB_reg_number = Company.randomNumberString(8)
            SC_reg_number = ['SC', Company.randomNumberString(6)]
            SC_reg_number = ''.join(SC_reg_number)
            OC_reg_number = ['OC', Company.randomNumberString(6)]
            OC_reg_number = ''.join(OC_reg_number)
            
            company_reg_number = random.choice([GB_reg_number, 
                                               GB_reg_number, 
                                               SC_reg_number, 
                                               OC_reg_number])
            return company_reg_number
        
        def SIC():
            """
            # SIC Code 
            # Make a random 4 digit string
            """
            SIC = Company.randomNumberString(4)
            return SIC
        
        def VAT_no():
            """
            # VAT number
            # GB and a 9 digit string
            """
            VAT_no = ['GB', Company.randomNumberString(9)]
            VAT_no = ''.join(VAT_no)
            return VAT_no 
        
        def year_business_started():
            """
            # Year Business Started
            # Business age in reasonable range
            # All UK businesses are older than 886 (the royal mint)
            """
            year_business_started = str(random.randrange(886, 2020))
            return year_business_started
        
        def additional_trad_style():
            """
            # Additional Trading Style
            # Generate an additional company name 1/7 times
            """
            add_company_styles = [str(gen_data.create_company_name()),'','','','','','']
            additional_trad_style = random.choice(add_company_styles)
            return additional_trad_style
        
        def tel_no():
            """
            # Telephone number
            # UK Phone numbers begin 0, then either 1,2 or 7 and are 11 digits
            # '0[127]{1}0[78]{1}[0-9]{8}$'
            # Reverse regex with exrex
            """
            tel_no = str(exrex.getone('0[127]{1}0[78]{1}[0-9]{8}$'))
            return tel_no
        
        def business_web_address():
            """
            # Business Website Address
            # Build a website name with company name
            """
            name = Company.Account.name_address()[1]
            www = "www."
            domain = name
            tld = ".co.uk"
            business_web_address = [www, domain, tld]
            business_web_address = ''.join(business_web_address)
            return business_web_address
        
        def poc_name():
            """
            # Point of Contact Name
            # Build a fake name
            """
            poc_name = Company.fake.name()
            print(1, poc_name)
            return poc_name
        
        def poc_email():
            """
            # Point of Contact Email Address
            # Using fake name to drive email creation
            """
            poc_name = Company.Account.poc_name()
            print(2, poc_name)
            poc_email = gen_data.create_email(name=poc_name,
                                              tld='fake_CDE.com')
            return poc_email
        
        def poc_tel():
            """
            # Point of Contact Telephone Number
            # Random Regex reverse UK tel number
            """
            poc_tel = str(exrex.getone('0[127]{1}0[78]{1}[0-9]{8}$'))
            return poc_tel
        
        def poc_job():
            """
            # Point of Contact Job Title
            # Random data generation based on US census data
            """
            poc_job = gen_data.create_job_title()
            return poc_job
        
        def par_com_name_address():
            """
            # Parent Company Name and Address
            # Make a name and address
            # Concatenate
            # 1/3 chance of having a parent company
            """
            name = str(gen_data.create_company_name())
            name = ''.join(name)
            address = Company.fake.address()
            address_split = address.split('\n')
            address_split.insert(0, name)
            par_com_name_address = ','.join(address_split)
            par_com_name_address = random.choice([par_com_name_address,'',''])
            return par_com_name_address 
        
        def par_com_reg_no():
            """
            # Parent Company Registered Number
            # If there's a parent company generate a random company reg number
            # Else it's blank
            """
            if Company.Account.par_com_name_address != '':
                GB_reg_number = Company.randomNumberString(8)
                SC_reg_number = ['SC', Company.randomNumberString(6)]
                SC_reg_number = ''.join(SC_reg_number)
                OC_reg_number = ['OC', Company.randomNumberString(6)]
                OC_reg_number = ''.join(OC_reg_number)
                
                par_com_reg_no = random.choice([GB_reg_number, 
                                                   GB_reg_number, 
                                                   SC_reg_number, 
                                                   OC_reg_number])
            else:
                par_com_reg_no = ''
            
            return par_com_reg_no
        
        def par_com_tel_no():
            """
            # Parent Company Telephone Number
            # If parent, make phone number
            """
            if Company.Account.par_com_name_address != '':
                par_com_tel_no = str(exrex.getone('0[127]{1}0[78]{1}[0-9]{8}$'))
            else:
                par_com_tel_no = ''
            
            return par_com_tel_no
        
        def par_com_VAT_no():
            """    
            # Parent Company VAT Number
            # if parent make VAT number
            """
            if Company.Account.par_com_name_address != '':
                par_com_VAT_no = str(exrex.getone('0[127]{1}0[78]{1}[0-9]{8}$'))
            else:
                par_com_VAT_no = ''
            
            return par_com_VAT_no
        
        def previous_name_address():
            """
            # If there has been a name change, generate a name and address
            """
            if Company.Account.name_change == 'Y':
                name = str(gen_data.create_company_name())
                name = ''.join(name)
                address = Company.fake.address()
                address_split = address.split('\n')
                address_split.insert(0, name)
                previous_name_address = ','.join(address_split)
                previous_name_address = previous_name_address
            else:
                previous_name_address = ''
            
            return previous_name_address
        
        def experinan_expin():
            """
            # Experian ExPin customer PIN
            # Always ''
            """
            experinan_expin = ''
            return experinan_expin 
        
        def person_type():
            """
            # Person Type Indicator
            # Pick a flag from the 4 flags
            """
            person_type = ['P','A','D','O']
            person_type = random.choice(person_type)
            return person_type
        
        def signatory_flag():
            """
            # Signatory on the Account Flag
            # If person type is O add POC name,
            # Else select either Y or N
            """
            if Company.Account.person_type != 'O':
                signatory_flag = Company.Account.poc_name()
            else:
                signatory_flag = ['Y','N']
                signatory_flag = random.choice(signatory_flag)
            return signatory_flag 
        
        def shareholders_flag():
            """
            # Shareholders Flag
            # Blank  
            """
            shareholders_flag = ''
            return shareholders_flag 
        
        def country_reg():
            """
            # Country of Registration
            # Hardcoded UK
            """
            country_reg = 'UK'
            return country_reg
        
        def DoB():
            """
            # Date of birth
            #'random date formatted ddmmyyyy'
            """
            DoB = Company.random_date('01011970', '31122010')
            return DoB
        
        def prop_dir_guarantee():
            """
            # Proprietors/Directors Guarantee
            # Blank
            """
            prop_dir_guarantee = ''
            return prop_dir_guarantee 
        
        def prop_dir_guarantee_can():
            """
            # Proprietors/Directors Guarantee Cancelled or discharged
            # Blank
            """
            prop_dir_guarantee_can = ''
            return prop_dir_guarantee_can 
        
    class Payment():
        def account_type():
            """
            #Account type 
            # Picks randomly from 5, 15, 2 and 3
            # Currently random picker, may need link to ODS data
            """
            account_types = ['5','15','2','3']
            account_type = random.choice(account_types)
            return account_type
        
        def start_date():
            """
            # Start date
            #'ddmmyyyy'
            """
            start_date = Company.random_date('01012010', '31122030')
            return start_date 
        
        def close_date():
            """
            # Close date
            #'ddmmyyyy'
            """
            close_date = Company.random_date(Company.Payment.start_date(), '31122030')
            return close_date 
        
        def payment_amount():
            """
            # Payment Amount
            # Random number between 1 and 1,000,000
            """
            payment_amount = str(random.randrange(1, 1000000))
            return payment_amount
        
        def repayment_period():
            """
            # Repayment Period
            # Pick a 3 month repayment period
            """
            repayment_period = Company.randomMonthPeroid()
            return repayment_period 
        
        def current_bal():
            """
            # Current balance
            # Random number between 1 and 1,000,000
            """
            current_bal = str(random.randrange(1, 1000000))
            return current_bal 
        
        def credit_bal_credit_ind():
            """
            # Credit Balance Credit Indicator
            # Choice between - and blank. 
            # Blank more likely to occur
            """
            credit_bal_credit_inds = ['-','','']
            credit_bal_credit_ind = random.choice(credit_bal_credit_inds)
            return credit_bal_credit_ind
        
        def staus_code():
            """
            # Status code
            # Random choice from ['U','D','0','1','2','3','4','5','6','8','S']
            """
            staus_codes = ['U','D','0','1','2','3','4','5','6','8','S']
            staus_code = random.choice(staus_codes)
            return staus_code 
        
        def spec_instruction_ind():
            """
            # Special Instruction Indicator
            # Blank
            """
            spec_instruction_ind = ''
            return spec_instruction_ind 
        
        def credit_limit():
            """
            # Credit Limit
            # If 15 or 5, select a number between 1 and 10,000
            # Else credit limit is 0
            """
            if Company.Payment.account_type() == '5' or Company.Payment.account_type() == '15':
                credit_limit = str(random.randrange(1, 10000))
                
            else:
                credit_limit = '0'
            return credit_limit 
            
        def flag_settings():
            """
            # Flag Settings
            # Flag Settings are Blank
            """
            flag_settings = ''
            return flag_settings 
        
        def debenture():
            """
            # Debenture 
            # Debenture is Blank
            """
            debenture = ''
            return debenture
        
        def mortgage_flag():
            """
            # Mortgage flag 
            # Comment ( Check the source code to derive the flag)
            # Therefore mortgage_flag is blank
            """
            mortgage_flag = ''
            return mortgage_flag 
        
        def airtimestatus_flag():
            """
            # Airtime Status Flag
            # Optional therefore blank
            """
            airtimestatus_flag = ''
            return airtimestatus_flag 
        
        def trans_to_collec_acc_flag():
            """
            # Transferred to Collection Account Flag
            # Action: Business (Ben) has to provide the business logic for this field.
            # Therefore blank
            """
            trans_to_collec_acc_flag = ''
            return trans_to_collec_acc_flag 
        
        def balance_type():
            """
            # Balance Type
            # Only for the Account Type 15  - If Average Balance
            # is provided in Current Balance field then Populate 'A' 
            # else Blank
            """
            if Company.Payment.account_type == '15':
                balance_types = ['','A']
                balance_type = random.choice(balance_types)
            else:
                balance_type = '0'
            return balance_type
        
        def credit_turnover():
            """
            # Credit Turnover
            # Optional and blank
            """
            credit_turnover = ''
            return credit_turnover 
        
        def primary_acc_indicator():
            """
            # Primary Account Indicator
            # Optional and blank
            """
            primary_acc_indicator = ''
            return primary_acc_indicator
        
        def default_satisfaction_date():
            """
            # Default Satisfaction Date
            # Optional and blank
            """
            default_satisfaction_date = ''
            return default_satisfaction_date 
        
        def rejection_flag():
            """
            # Rejection Flag
            # if account type = 15, select one of the rejection flags
            # Else, blank?
            
            Currently hard coded as 1...
            """
#            if Company.Payment.account_type() == '15':
#                rejection_flags = ['866','873','875','867','874','876','868','0','1']
#                rejection_flag = random.choice(rejection_flags)
#            else:
#                rejection_flag = ''
            return 1
        
        def bank_sort_code():
            """
            # Bank sort code
            # Hardcoded as Metro sort code
            """
            bank_sort_code  = '230580'
            return bank_sort_code  
        
        def original_default_bal():
            """
            # Original Default Balance
            # (This field should only be populated when the Account Status is 8 (default). 
            # Where the Account Status is not 8, this field should be filled with zeros)
            """
            if Company.Payment.staus_code() == '8':
                original_default_bal = Company.Payment.current_bal()
            else:
                original_default_bal = '0000000'
            return original_default_bal 
        
        def payment_freq_indicator():
            """
            # Payment Frequency Indicator
            # Mandatory, Hardcode 'M', example data W????
            # Only applicable for Credit Cards Account Type 05.???? Contradictory!
            # Hardcoded to M...
            """
            payment_freq_indicator = 'M'
            return payment_freq_indicator 
        
        def no_credit_cards():
            """
            # Number of Credit Cards issued
            # Random number between 1 and 100
            """
            no_credit_cards = str(random.randrange(1, 100))
            return no_credit_cards 
            
        def repayment_amount():
            """
            # Repayment Amount
            # Random number between 1 and 100,000
            """
            repayment_amount = str(random.randrange(0, 100000))
            return repayment_amount 
            
        def repayment_amount_pay_cred_ind():
            """
            # Repayment Amount Payment Credit Indicator
            # Optional and blank
            """
            repayment_amount_pay_cred_ind = ''
            return repayment_amount_pay_cred_ind 
        
        def prev_bal():
            """
            # Previous Statement Balance
            # Optional and blank
            """
            prev_bal = ''
            return prev_bal 
        
        def prev_bal_ind():
            """
            # Previous Statement Balance Indicator
            # Optional and blank
            """
            prev_bal_ind = ''
            return prev_bal_ind 
        
        def no_of_cash_advances():
            """
            # Number of Cash Advances
            # Optional and blank
            """
            no_of_cash_advances = ''
            return no_of_cash_advances 
        
        def value_of_cash_advances():
            """
            # Value of Cash Advances
            # Optional and blank
            """
            value_of_cash_advances = ''
            return value_of_cash_advances 
        
        def payment_code():
            """
            # Payment Code
            # Optional and blank
            """
            payment_code = ''
            return payment_code 
        
        def promotion_activity_flag():
            """
            # Promotion Activity Flag
            # Optional and blank
            """
            promotion_activity_flag = ''
            return promotion_activity_flag 
        
        def payment_type():
            """
            # Payment Type
            # Hard coded as C
            """
            payment_type = 'C'
            return payment_type 
        
        def new_acc_no():
            """
            # New Account Number
            # Optional and blank
            """
            new_acc_no = ''
            return new_acc_no 
        
        def new_person_no():
            """
            # New Person Number
            # Optional and blank
            """
            new_person_no = ''
            return new_person_no
        
    class Trailer():
        def trailer_ID():
            """
            # Trailer Identifier
            # Blank
            """
            trailer_ID = ''
            return trailer_ID 
        
        def total_records(loop_counter):
            """
            # Total number of records
            """
            total_records = str(loop_counter)
            return total_records 



        
#Write to csv
with open('CAIS_Demo.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    loop_counter = 100
    for i in range(1, loop_counter):
        nthcompany = Company
        if i == 1:
            combination_wrtier.writerow(nthcompany.headers())
        else:
            company_info = [nthcompany.Header.Header(),
                            nthcompany.Header.source_code(),
                            nthcompany.Header.creation_date(),
                            nthcompany.Header.name_portfolio(),
                            nthcompany.Header.filler(),
                            nthcompany.Header.CAIS_version_indicator(),
                            nthcompany.Header.credit_card_bah_flag(),
                            nthcompany.Header.filler(),
                            nthcompany.Account.account_number(),
                            nthcompany.Account.person_number(),
                            nthcompany.Account.business_type_indicator(),
                            nthcompany.Account.name_address()[0],
                            nthcompany.Account.address_type(),
                            nthcompany.Account.name_change(),
                            nthcompany.Account.company_reg_number(),
                            nthcompany.Account.SIC(),
                            nthcompany.Account.VAT_no(),
                            nthcompany.Account.year_business_started(),
                            nthcompany.Account.additional_trad_style(),
                            nthcompany.Account.tel_no(),
                            nthcompany.Account.business_web_address(),
                            nthcompany.Account.poc_name(),
                            nthcompany.Account.poc_email(),
                            nthcompany.Account.poc_tel(),
                            nthcompany.Account.poc_job(),
                            nthcompany.Account.par_com_name_address(),
                            nthcompany.Account.par_com_reg_no(),
                            nthcompany.Account.par_com_tel_no(),
                            nthcompany.Account.par_com_VAT_no(),
                            nthcompany.Account.previous_name_address(),
                            nthcompany.Account.experinan_expin(),
                            nthcompany.Account.person_type(),
                            nthcompany.Account.signatory_flag(),
                            nthcompany.Account.shareholders_flag(),
                            nthcompany.Account.country_reg(),
                            nthcompany.Account.DoB(),
                            nthcompany.Account.prop_dir_guarantee(),
                            nthcompany.Account.prop_dir_guarantee_can(),
                            nthcompany.Header.filler(),
                            nthcompany.Payment.account_type(),
                            nthcompany.Payment.start_date(),
                            nthcompany.Payment.close_date(),
                            nthcompany.Payment.payment_amount(),
                            nthcompany.Payment.repayment_period(),
                            nthcompany.Payment.current_bal(),
                            nthcompany.Payment.credit_bal_credit_ind(),
                            nthcompany.Payment.staus_code(),
                            nthcompany.Payment.spec_instruction_ind(),
                            nthcompany.Payment.credit_limit(),
                            nthcompany.Payment.flag_settings(),
                            nthcompany.Payment.debenture(),
                            nthcompany.Payment.mortgage_flag(),
                            nthcompany.Payment.airtimestatus_flag(),
                            nthcompany.Payment.trans_to_collec_acc_flag(),
                            nthcompany.Payment.balance_type(),
                            nthcompany.Payment.credit_turnover(),
                            nthcompany.Payment.primary_acc_indicator(),
                            nthcompany.Payment.default_satisfaction_date(),
                            nthcompany.Payment.rejection_flag(),
                            nthcompany.Payment.bank_sort_code(),
                            nthcompany.Payment.original_default_bal(),
                            nthcompany.Payment.payment_freq_indicator(),
                            nthcompany.Payment.no_credit_cards(),
                            nthcompany.Payment.repayment_amount(),
                            nthcompany.Payment.repayment_amount_pay_cred_ind(),
                            nthcompany.Payment.prev_bal(),
                            nthcompany.Payment.prev_bal_ind(),
                            nthcompany.Payment.no_of_cash_advances(),
                            nthcompany.Payment.value_of_cash_advances(),
                            nthcompany.Payment.payment_code(),
                            nthcompany.Payment.promotion_activity_flag(),
                            nthcompany.Payment.payment_type(),
                            nthcompany.Payment.new_acc_no(),
                            nthcompany.Payment.new_person_no(),
                            nthcompany.Trailer.trailer_ID(),
                            nthcompany.Trailer.total_records(loop_counter),
                            nthcompany.Header.filler()]
            combination_wrtier.writerow((company_info))
            