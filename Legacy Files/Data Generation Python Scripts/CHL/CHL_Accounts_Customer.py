# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 10:46:36 2020

@author: mmorganwilliams
"""

import csv
import os
import datetime
import random
import string
import exrex
import pprint
from faker import Faker
from barnum import gen_data



## Define functions for random generation of data

def randomString(stringLength):
    """
    Join letters together for a random string of defined length
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))

def randomNumberString(stringLength):
    """
    Join letters together for a random string of numbers of defined length
    """
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(stringLength))

def random_date(fake_var, start, end, date_format='%d%m%Y'):
    """
    Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    The returned time will be in the specified format.
    """
    date = fake_var.date_time_between(start_date=datetime.datetime.strptime(start, date_format),
                                      end_date=datetime.datetime.strptime(end, date_format),
                                      tzinfo=None)
    date = datetime.datetime.strftime(date, date_format)

    return date

def randomMonthPeroid():
    """
    Select from random month periods between 3 and 48 months,
    Return a string
    """
    month_periods = ['3', '6', '12', '18', '24', '36', '48']
    return random.choice(month_periods)

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

def csv_array_reader(csv_file):
    """
    Read in a single column csv as an array
    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            list_csv.append(row[0])

    return list_csv


def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_industries():
    """
    read in all industries and SIC codes from csv to dict
    """
    CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
    Industry_dict = csv_dict_reader(CSV_FILE)
    return Industry_dict

def random_industry(Industry_dict):
    """
    Using dictionary keys (SIC) pick a random industry and return
    SIC and Industry
    """
    keys = list(Industry_dict.keys())
    random_ind = random.choice(keys)
    return [random_ind, Industry_dict[random_ind]]

def get_account_nos():
    """
    Get all account numbers from csv
    """
    CSV_FILE = os.path.join('Config', 'Account_Nos.csv')
    return csv_array_reader(CSV_FILE)

def random_account_no(all_account_nos):
    """
    Select an account numbers from array
    """
    return random.choice(all_account_nos)

def fake_address(fake_var, Counties):
    """
    Prerequists:
    Instantiate Faker for UK addresses and names
    Read in file of UK counties
    Run:
    Gather address from faker
    split it
    Add a county
    Return it
    """
    address = fake_var.address()
    address = address.split('\n')
    county = random_UK_Counties(Counties)
    address.insert(-1, county)
    return address

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    -
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def create_job_title():
    """
    Make a job title using Barnum
    """
    return gen_data.create_job_title()

class CHL_headers():
    """
    Class for all CHL headers
    """
    def __init__(self):
        """
        Setting headers for each file as attributes
        """
        Accounts_csv_file = os.path.join('Config', 'CHL_ACCOUNTS_HEADERS.csv')
        self.accounts_headers = csv_array_reader(Accounts_csv_file)

        Customer_csv_file = os.path.join('Config', 'CHL_CUSTOMERS_HEADERS.csv')
        self.customers_headers = csv_array_reader(Customer_csv_file)


class CHL_accounts_fields():
    """
    All accounts fields as a class
    """

    def RUNID(self):
        """
        Create RUNID field
        38 charatcter number string
        """
        return randomNumberString(38)

    def REC_TIME(self, fake_var):
        """
        Create rec time field
        Random date in the last 3 months
        """
        return random_date(fake_var, '01122019', '28022020')

    def ACCOUNTID(self):
        """
        Create account id field
        19 charatcter number string
        """
        return randomNumberString(19)

    def CMS(self):
        """
        Set cms field
        Random number string 14 characters long
        """
        return randomNumberString(14)

    def CUSTOMERID(self):
        """
        Set customer ID field
        Random number string 19 characters
        """
        return randomNumberString(19)

    def INCEPTIONDATE(self, fake_var):
        """
        Set inception date, random date in last 3 months
        """
        return random_date(fake_var, '01122019', '28022020')

    def TERMMONTHS(self):
        """
        Set a random term in months (3 month periodised)
        """
        return randomMonthPeroid()

    def TOTALCURRENTBALANCE(self):
        """
        Pick a number for total balence
        """
        return randomNumberString(7)

class CHL_customer_fields():
    """
    All customer fields as a class
    """

    def __init__(self):
        self.address = ''
        self.name = ''
        self.job_title = ''
        self.industry = ''

    def BANKRUPTCYDISCHARGEDDATE(self, fake_var):
        return random.choice([''*6,
                              random_date(fake_var, '01122019', '28022020')])
    def DATEOFBIRTH(self, fake_var):
        return random_date(fake_var, '01121950', '31122000')

    def DATEOFDEATH(self, fake_var):
        return random.choice([''*7,
                              random_date(fake_var, '01122019', '28022020')])

    def NUMBEROFCCJS(self):
        return random.choice([''*7,
                              randomNumberString(1)])

    def VALUEOFCCJS(self):
        return ''

    def BANKRUPTCYSTATUS(self):
        return ''

    def BORROWERINCOME(self):
        return randomNumberString(5)

    def COUNTY(self):
        return self.address[-2]

    def DISTRICT(self):
        return self.address[-3]

    def EMPLOYMENTDESCRIPTION(self):
        return self.job_title

    def EMPLOYMENTTYPE(self):
        return self.industry[1]

    def FIRSTNAME(self):
        return self.name[1]

    def GENDER(self):
        return self.name[-1]

    def INITIALS(self):
        return self.name[1][0]+self.name[2][0]

    def IVASTATUS(self):
        return ''

    def MARITALSTATUS(self):
        return ''

    def NATIONALINSURANCENO(self):
        return exrex.getone("^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$")

    def ORIGINATORCUSTOMERID(self):
        return ''

    def POSTCODE(self):
        return self.address[-1]

    def PREFCONTACTMETHOD(self):
        Contact_Methods = ['PHONE', 'EMAIL', 'MOBILE', 'FAX']
        return random.choice(Contact_Methods)

    def STREET(self):
        return self.address[0]

    def SURNAME(self):
        return self.name[2]

    def TITLE(self):
        return self.name[0]

    def TOWN(self):
        return self.address[1]

    def NATIONALITY(self):
        return 'GB'

    def NUMBEROFDEPENDANTS(self):
        return random.choice(['0', '1', '2', '2', '2', '3', '4', '5'])

class CHL():
    def __init__(self, fake_var, Counties, Industry_dict):
        #Derriveed reused fields
        self.name = fake_name(fake_var)
        self.address = fake_address(fake_var, Counties)
        self.industry = random_industry(Industry_dict)
        self.job_title = create_job_title()

        #Accounts fields
        self.runid = CHL_accounts_fields.RUNID(self)
        self.rec_time = CHL_accounts_fields.RUNID(self)
        self.accountid = CHL_accounts_fields.ACCOUNTID(self)
        self.cms = CHL_accounts_fields.CMS(self)
        self.customerid = CHL_accounts_fields.CUSTOMERID(self)
        self.inceptiondate = CHL_accounts_fields.INCEPTIONDATE(self, fake_var)
        self.termmonths = CHL_accounts_fields.TERMMONTHS(self)
        self.totalcurrentbalance = CHL_accounts_fields.TOTALCURRENTBALANCE(self)

        #Customers fields
        self.bankruptcydischargeddate = CHL_customer_fields.BANKRUPTCYDISCHARGEDDATE(self, fake_var)
        self.dateofbirth = CHL_customer_fields.DATEOFBIRTH(self, fake_var)
        self.dateofdeath = CHL_customer_fields.DATEOFDEATH(self, fake_var)
        self.numberofccjs = CHL_customer_fields.NUMBEROFCCJS(self)
        self.valueofccjs = CHL_customer_fields.VALUEOFCCJS(self)
        self.bankruptcystatus = CHL_customer_fields.BANKRUPTCYSTATUS(self)
        self.borrowerincome = CHL_customer_fields.BORROWERINCOME(self)
        self.county = CHL_customer_fields.COUNTY(self)
        self.district = CHL_customer_fields.DISTRICT(self)
        self.employmentdescription = CHL_customer_fields.EMPLOYMENTDESCRIPTION(self)
        self.employmenttype = CHL_customer_fields.EMPLOYMENTTYPE(self)
        self.firstname = CHL_customer_fields.FIRSTNAME(self)
        self.gender = CHL_customer_fields.GENDER(self)
        self.initials = CHL_customer_fields.INITIALS(self)
        self.ivastatus = CHL_customer_fields.IVASTATUS(self)
        self.maritalstatus = CHL_customer_fields.MARITALSTATUS(self)
        self.nationalinsuranceno = CHL_customer_fields.NATIONALINSURANCENO(self)
        self.originatorcustomerid = CHL_customer_fields.ORIGINATORCUSTOMERID(self)
        self.postcode = CHL_customer_fields.POSTCODE(self)
        self.prefcontactmethod = CHL_customer_fields.PREFCONTACTMETHOD(self)
        self.street = CHL_customer_fields.STREET(self)
        self.surname = CHL_customer_fields.SURNAME(self)
        self.title = CHL_customer_fields.TITLE(self)
        self.town = CHL_customer_fields.TOWN(self)
        self.nationality = CHL_customer_fields.NATIONALITY(self)
        self.numberofdependants = CHL_customer_fields.NUMBEROFDEPENDANTS(self)

LOOP_COUNTER = 1000
COMPANY_DICT = {}

UK_COUNTIES = get_UK_Counties()
INDUSTRIES = get_industries()

FAKE = Faker('en_GB')

for i in range(LOOP_COUNTER):
    nthCompany = CHL(FAKE, UK_COUNTIES, INDUSTRIES)
    COMPANY_DICT[i] = []

    ACCOUNTS_FIELDS = [nthCompany.runid,
                       nthCompany.rec_time,
                       nthCompany.accountid,
                       '',
                       '',
                       '',
                       nthCompany.cms,
                       '',
                       nthCompany.customerid,
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       nthCompany.inceptiondate,
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       nthCompany.accountid,
                       '',
                       '',
                       '',
                       nthCompany.termmonths,
                       nthCompany.totalcurrentbalance,
                       ]

    CHL_CUSTOMERS_FIELDS = [nthCompany.runid,
                            nthCompany.rec_time,
                            nthCompany.bankruptcydischargeddate,
                            nthCompany.customerid,
                            nthCompany.dateofbirth,
                            nthCompany.dateofdeath,
                            nthCompany.numberofccjs,
                            nthCompany.valueofccjs,
                            nthCompany.bankruptcystatus,
                            nthCompany.borrowerincome,
                            nthCompany.county,
                            nthCompany.district,
                            nthCompany.employmentdescription,
                            nthCompany.employmenttype,
                            nthCompany.firstname,
                            nthCompany.gender,
                            nthCompany.initials,
                            nthCompany.ivastatus,
                            nthCompany.maritalstatus,
                            nthCompany.nationalinsuranceno,
                            nthCompany.originatorcustomerid,
                            nthCompany.postcode,
                            nthCompany.prefcontactmethod,
                            nthCompany.street,
                            nthCompany.surname,
                            nthCompany.title,
                            nthCompany.town,
                            nthCompany.nationality,
                            nthCompany.numberofdependants
                            ]

    COMPANY_DICT[i].append(ACCOUNTS_FIELDS)
    COMPANY_DICT[i].append(CHL_CUSTOMERS_FIELDS)

customer_header_instance = CHL_headers()
pprint.pprint(customer_header_instance.accounts_headers)

#Write to csv
with open('CHL_Accounts_Demo.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for line_No, (dict_key, value) in enumerate(COMPANY_DICT.items()):
        if line_No == 0:
            customer_header_instance = CHL_headers()
            combination_wrtier.writerow(customer_header_instance.accounts_headers)
        else:
            combination_wrtier.writerow(value[0])
            
with open('CHL_Customer_Demo.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for line_No, (dict_key, value) in enumerate(COMPANY_DICT.items()):
        if line_No == 0:
            customer_header_instance = CHL_headers()
            combination_wrtier.writerow(customer_header_instance.customers_headers)
        else:
            combination_wrtier.writerow(value[1])
            