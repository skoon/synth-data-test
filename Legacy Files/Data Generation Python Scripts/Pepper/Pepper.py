# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 15:56:30 2020

@author: mmorganwilliams
"""

import csv
import os
import datetime
import random
import string
import exrex
import pprint
from faker import Faker
from barnum import gen_data



## Define functions for random generation of data

def randomString(stringLength):
    """
    Join letters together for a random string of defined length
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))

def randomNumberString(stringLength):
    """
    Join letters together for a random string of numbers of defined length
    """
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(stringLength))

def random_date(fake_var, start, end, date_format='%d%m%Y'):
    """
    Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    The returned time will be in the specified format.
    """
    date = fake_var.date_time_between(start_date=datetime.datetime.strptime(start, date_format),
                                      end_date=datetime.datetime.strptime(end, date_format),
                                      tzinfo=None)
    date = datetime.datetime.strftime(date, date_format)

    return date

def randomMonthPeroid():
    """
    Select from random month periods between 3 and 48 months,
    Return a string
    """
    month_periods = ['3', '6', '12', '18', '24', '36', '48']
    return random.choice(month_periods)

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

def csv_array_reader(csv_file):
    """
    Read in a single column csv as an array
    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            list_csv.append(row[0])

    return list_csv


def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_industries():
    """
    read in all industries and SIC codes from csv to dict
    """
    CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
    Industry_dict = csv_dict_reader(CSV_FILE)
    return Industry_dict

def random_industry(Industry_dict):
    """
    Using dictionary keys (SIC) pick a random industry and return
    SIC and Industry
    """
    keys = list(Industry_dict.keys())
    random_ind = random.choice(keys)
    return [random_ind, Industry_dict[random_ind]]

def get_account_nos():
    """
    Get all account numbers from csv
    """
    CSV_FILE = os.path.join('Config', 'Account_Nos.csv')
    return csv_array_reader(CSV_FILE)

def random_account_no(all_account_nos):
    """
    Select an account numbers from array
    """
    return random.choice(all_account_nos)

def fake_address(fake_var, Counties):
    """
    Prerequists:
    Instantiate Faker for UK addresses and names
    Read in file of UK counties
    Run:
    Gather address from faker
    split it
    Add a county
    Return it
    """
    address = fake_var.address()
    address = address.split('\n')
    county = random_UK_Counties(Counties)
    address.insert(-1, county)
    return address

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    -
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def create_job_title():
    """
    Make a job title using Barnum
    """
    return gen_data.create_job_title()

class Pepper_headers():
    """
    Class for all CHL headers
    """
    def __init__(self):
        """
        Setting headers for each file as attributes
        """
        csv_file = os.path.join('Config', 'pepper_accounts_headers.csv')
        self.pepper_accounts_headers = csv_array_reader(csv_file)
        
        
        csv_file = os.path.join('Config', 'pepper_links_headers.csv')
        self.pepper_links_headers = csv_array_reader(csv_file)
        
        csv_file = os.path.join('Config', 'pepper_customers_headers.csv')
        self.pepper_customers_headers = csv_array_reader(csv_file)
        



class pepper():
    """
    All accounts fields as a class
    """
    def RUNID1(self):
        return randomNumberString(8)
    def REC_TIME1(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    def ACCOUNTID(self):
        return randomNumberString(8)
    def PRIMARYACCOUNTID(self):
        return randomNumberString(8)
    def ORIGINATORACCOUNTID(self):
        return randomNumberString(8)
    def INCEPTIONDATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
        
    def CMS(self):
        return randomNumberString(8)
    
    def RUNID2(self):
        return randomNumberString(8)
    def REC_TIME2(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
        
    def REFERENCEID(self):
        return randomNumberString(8)
    
    def CAISSUFFIX(self):
        return randomNumberString(8)
    
    def REFERENCETYPE(self):
        return randomNumberString(8)
    
    
    def RUNID3(self):
        return randomNumberString(8)
    
    def REC_TIME3(self, fake_var):
        return random_date(fake_var, '01121950', '31122000')
    
    def CUSTOMERID(self):
        return randomNumberString(8)
    
    def DATEOFBIRTH(self, fake_var):
        return random_date(fake_var, '01121960', '31122000')

    def DATEOFDEATH(self, fake_var):
        return random.choice([''*7,
                              random_date(fake_var, '01122019', '28022020')])
    
    def BANKRUPTCYDISCHARGEDDATE(self, fake_var):
        return random.choice([''*6,
                              random_date(fake_var, '01122019', '28022020')])
    
    def NUMBEROFCCJS(self):
        return ''
    def VALUEOFCCJS(self):
        return ''
    
    def ORIGINATORCUSTOMERID(self):
        return ''
    
    def SURNAME(self):
        return self.name[2]
    def FIRSTNAME(self):
        return self.name[1]
    def INITIALS(self):
        return ''.join([self.name[1],self.name[2]])
    
    def TITLE(self):
        return self.name[1]
    
    def GENDER(self):
        return self.name[-1]    
    
    def PREFCONTACTMETHOD(self):
        Contact_Methods = ['PHONE', 'EMAIL', 'MOBILE', 'FAX']
        return random.choice(Contact_Methods)    
    
    def STREET(self):
        return self.address[0]
        
    def DISTRICT(self):
        return self.address[-2]
        
    def TOWN(self):
        return self.address[1]
        
    def COUNTY(self):
        return self.address[2]
        
    def POSTCODE(self):
        return self.address[-1]

    def EMPLOYMENTDESCRIPTION(self):
        return self.job_title
        
    def EMPLOYMENTTYPE(self):
        return self.industry[1]
        
    def BORROWERINCOME(self):
        return randomNumberString(4)
        
    def IVASTATUS(self):
        return '[Unknown]'
    
    def BANKRUPTCYSTATUS(self):
        return '[Unknown]'
    
    def MARITALSTATUS(self):
        return ''
    
    def NATIONALINSURANCENO(self):
        return exrex.getone("^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$")

    
class pepper_customer():
    def __init__(self, fake_var, Counties, Industry_dict):
        self.name = fake_name(fake_var)
        self.address = fake_address(fake_var, Counties)
        self.industry = random_industry(Industry_dict)
        self.job_title = create_job_title()
        
        
        self.runid1 = pepper.RUNID1(self)
        self.rec_time1 = pepper.REC_TIME1(self, fake_var)
        self.accountid = pepper.ACCOUNTID(self)
        self.primaryaccountid = pepper.PRIMARYACCOUNTID(self)
        self.originatoraccountid = pepper.ORIGINATORACCOUNTID(self)
        self.inceptiondate = pepper.INCEPTIONDATE(self, fake_var)
        self.cms = pepper.CMS(self)
        
        
        self.runid2 = pepper.RUNID2(self)
        self.rec_time2 = pepper.REC_TIME2(self, fake_var)
        self.accountid = pepper.ACCOUNTID(self)
        self.referenceid = pepper.REFERENCEID(self)
        self.caissuffix = pepper.CAISSUFFIX(self)
        self.referencetype = pepper.REFERENCETYPE(self)
        
        
        self.runid3 = pepper.RUNID3(self)
        self.rec_time3 = pepper.REC_TIME3(self, fake_var)
        self.customerid = pepper.CUSTOMERID(self)
        self.dateofbirth = pepper.DATEOFBIRTH(self, fake_var)
        self.dateofdeath = pepper.DATEOFDEATH(self, fake_var)
        self.bankruptcydischargeddate = pepper.BANKRUPTCYDISCHARGEDDATE(self, fake_var)
        self.numberofccjs = pepper.NUMBEROFCCJS(self)
        self.valueofccjs = pepper.VALUEOFCCJS(self)
        self.originatorcustomerid = pepper.ORIGINATORCUSTOMERID(self)
        self.surname = pepper.SURNAME(self)
        self.firstname = pepper.FIRSTNAME(self)
        self.initials = pepper.INITIALS(self)
        self.title = pepper.TITLE(self)
        self.gender = pepper.GENDER(self)
        self.prefcontactmethod = pepper.PREFCONTACTMETHOD(self)
        self.street = pepper.STREET(self)
        self.district = pepper.DISTRICT(self)
        self.town = pepper.TOWN(self)
        self.county = pepper.COUNTY(self)
        self.postcode = pepper.POSTCODE(self)
        self.employmentdescription = pepper.EMPLOYMENTDESCRIPTION(self)
        self.employmenttype = pepper.EMPLOYMENTTYPE(self)
        self.borrowerincome = pepper.BORROWERINCOME(self)
        self.ivastatus = pepper.IVASTATUS(self)
        self.bankruptcystatus = pepper.BANKRUPTCYSTATUS(self)
        self.maritalstatus = pepper.MARITALSTATUS(self)
        self.nationalinsuranceno = pepper.NATIONALINSURANCENO(self)


LOOP_COUNTER = 1000
COMPANY_DICT = {}

UK_COUNTIES = get_UK_Counties()
INDUSTRIES = get_industries()

FAKE = Faker('en_GB')

for i in range(LOOP_COUNTER):
    nthCompany = pepper_customer(FAKE, UK_COUNTIES, INDUSTRIES)
    COMPANY_DICT[i] = []

    PEPPER_ACCOUNTS = [nthCompany.runid1,
                        nthCompany.rec_time1,
                        nthCompany.accountid,
                        nthCompany.primaryaccountid,
                        nthCompany.originatoraccountid,
                        nthCompany.inceptiondate,
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        nthCompany.cms
                        ]

    PEPPER_LINKS = [nthCompany.runid2,
                        nthCompany.rec_time2,
                        nthCompany.accountid,
                        nthCompany.referenceid,
                        nthCompany.caissuffix,
                        nthCompany.referencetype
                        ]
    
    PEPPER_CUSTOMERS = [nthCompany.runid3,
                        nthCompany.rec_time3,
                        nthCompany.customerid,
                        nthCompany.dateofbirth,
                        nthCompany.dateofdeath,
                        nthCompany.bankruptcydischargeddate,
                        nthCompany.numberofccjs,
                        nthCompany.valueofccjs,
                        nthCompany.originatorcustomerid,
                        nthCompany.surname,
                        nthCompany.firstname,
                        nthCompany.initials,
                        nthCompany.title,
                        nthCompany.gender,
                        nthCompany.prefcontactmethod,
                        nthCompany.street,
                        nthCompany.district,
                        nthCompany.town,
                        nthCompany.county,
                        nthCompany.postcode,
                        nthCompany.employmentdescription,
                        nthCompany.employmenttype,
                        nthCompany.borrowerincome,
                        nthCompany.ivastatus,
                        nthCompany.bankruptcystatus,
                        nthCompany.maritalstatus,
                        nthCompany.nationalinsuranceno,
                        ]


    COMPANY_DICT[i].append(PEPPER_ACCOUNTS)
    COMPANY_DICT[i].append(PEPPER_LINKS)
    COMPANY_DICT[i].append(PEPPER_CUSTOMERS)

customer_header_instance = Pepper_headers()

#Write to csv
with open('Pepper Accounts.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for line_No, (dict_key, value) in enumerate(COMPANY_DICT.items()):
        if line_No == 0:
            header_instance = Pepper_headers()
            combination_wrtier.writerow(customer_header_instance.pepper_accounts_headers)
        else:
            combination_wrtier.writerow(value[0])
            
with open('Pepper Links.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for line_No, (dict_key, value) in enumerate(COMPANY_DICT.items()):
        if line_No == 0:
            header_instance = Pepper_headers()
            combination_wrtier.writerow(customer_header_instance.pepper_links_headers)
        else:
            combination_wrtier.writerow(value[1])

with open('Pepper Customers.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for line_No, (dict_key, value) in enumerate(COMPANY_DICT.items()):
        if line_No == 0:
            header_instance = Pepper_headers()
            combination_wrtier.writerow(customer_header_instance.pepper_customers_headers)
        else:
            combination_wrtier.writerow(value[2])
