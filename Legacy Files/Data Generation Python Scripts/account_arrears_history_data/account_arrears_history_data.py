# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 11:23:07 2020

@author: mmorganwilliams
"""
import csv
import itertools
import os
import datetime
import random
import string
import exrex
import time
from faker import Faker
from barnum import gen_data

def randomString(stringLength):
    """
    Join letters together for a random string of defined length
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))

def randomNumberString(stringLength):
    """
    Join letters together for a random string of numbers of defined length
    """
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(stringLength))

def random_date(fake_var, start, end, date_format='%d%m%Y'):
    """
    Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    The returned time will be in the specified format.
    """
    date = fake_var.date_time_between(start_date=datetime.datetime.strptime(start, date_format),
                                      end_date=datetime.datetime.strptime(end, date_format),
                                      tzinfo=None)
    date = datetime.datetime.strftime(date, date_format)

    return date

def randomMonthPeroid():
    """
    Select from random month periods between 3 and 48 months,
    Return a string
    """
    month_periods = ['3', '6', '12', '18', '24', '36', '48']
    return random.choice(month_periods)

def csv_dict_reader(csv_file):
    """
    Read in a 2 columns csv and store as a dictionary
    Use column 1 as the key
    """
    dict_csv = {}
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            dict_csv[row[0]] = row[1]
    return dict_csv

def csv_array_reader(csv_file):
    """
    Read in a single column csv as an array
    """
    list_csv = []
    with open(csv_file) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            list_csv.append(row[0])

    return list_csv


def get_UK_Counties():
    """
    Read in the single column csv  for UK counties as an array
    """
    CSV_FILE = os.path.join('Config', 'UK_Counties.csv')
    Counties = csv_array_reader(CSV_FILE)
    return Counties

def random_UK_Counties(Counties):
    """
    Pick one at random from the array as an array
    """
    random_county = random.choice(Counties)
    return random_county

def get_industries():
    """
    read in all industries and SIC codes from csv to dict
    """
    CSV_FILE = os.path.join('Config', 'SIC_Industries.csv')
    Industry_dict = csv_dict_reader(CSV_FILE)
    return Industry_dict

def random_industry(Industry_dict):
    """
    Using dictionary keys (SIC) pick a random industry and return
    SIC and Industry
    """
    keys = list(Industry_dict.keys())
    random_ind = random.choice(keys)
    return [random_ind, Industry_dict[random_ind]]

def get_account_nos():
    """
    Get all account numbers from csv
    """
    CSV_FILE = os.path.join('Config', 'Account_Nos.csv')
    return csv_array_reader(CSV_FILE)

def random_account_no(all_account_nos):
    """
    Select an account numbers from array
    """
    return random.choice(all_account_nos)

def fake_address(fake_var, Counties):
    """
    Prerequists:
    Instantiate Faker for UK addresses and names
    Read in file of UK counties
    Run:
    Gather address from faker
    split it
    Add a county
    Return it
    """
    address = fake_var.address()
    address = address.split('\n')
    county = random_UK_Counties(Counties)
    address.insert(-1, county)
    return address

def fake_name(fake_var):
    """
    Generate a fake name and gender
    -Run faker
    -Generate a fake prefix
    -Use the prefix as a driver for name creation from gendered names
    -
    """
    fake_prefix = [fake_var.prefix(), fake_var.prefix_female(), fake_var.prefix_male()]
    fake_prefix = random.choice(fake_prefix)

    if fake_prefix in ['Miss.', 'Mrs.', 'Miss', 'Mrs', 'Ms', 'Ms.']:
        fake_first_name = fake_var.first_name_female()
        GENDER = random.choice(['FEMALE', 'NON BINARY'])

    elif fake_prefix in ['Mr', 'Mr.']:
        fake_first_name = fake_var.first_name_male()
        GENDER = random.choice(['MALE', 'NON BINARY'])

    else:
        fake_first_name = fake_var.first_name()
        GENDER = random.choice(['MALE', 'FEMALE', 'NON BINARY'])

    fake_last_name = fake_var.last_name()

    return [fake_prefix, fake_first_name, fake_last_name, GENDER]

def create_job_title():
    """
    Make a job title using Barnum
    """
    return gen_data.create_job_title()


class SETL_STMT_headers():
    """
    Class for all CHL headers
    """
    def __init__(self):
        """
        Setting headers for each file as attributes
        """
        SETL_STMT_csv_file = os.path.join('Config', 
                                      'account_arrears_history_data_headers.csv')
        self.setl_stmt_headers = csv_array_reader(SETL_STMT_csv_file)
        
class SETL_STMT_fields():
    def __init__(self):
        self.address = ''
        self.name = ''
        self.job_title = ''
        self.industry = ''
        
    def CREATIONDATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def REPORTINGREFERENCEDATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def RUNID(self):
        return randomNumberString(8)
    
    def REC_TIME(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def PRODUCT_DESCRIPTION(self):
        return ''
    
    def CURRENCY(self):
        return random.choice(['GBP','GBP','GBP','GBP','USD','EUR'])
    
    def ACCOUNT_STATUS(self):
        return ''
    
    def EXPOSURE_IN_DEFAULT(self):
        return '' 
    
    def WATCHLIST_INDICATOR(self):
        return ''
    
    def ACCOUNT(self, account_nos):
        return random.choice(account_nos)
    
    def PRODUCT(self):
        return '' 
    
    def CUSTOMER_ID(self):
        return randomNumberString(8)
    
    def POSTING_RESTRICTION(self):
        return ''
    
    def ACCOUNT_STATUS_DATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def ACCOUNT_BALANCE(self):
        return randomNumberString(3)
    
    def CURRENT_MONTH_DUE_AMOUNT(self):
        return randomNumberString(2)
    
    def OVERDUE_AMOUNT(self):
        return randomNumberString(2)
    
    def TOTAL_DUE_AMOUNT(self):
        return randomNumberString(2)
    
    def CAPITAL(self):
        return randomNumberString(2)
    
    def FEES(self):
        return randomNumberString(2)
    
    def INTEREST(self):
        return randomNumberString(2)
    
    def DAYS_OVERDUE(self):
        return randomNumberString(2)
    
    def DATE_LAST_PYMT_RCVD(self):
        return randomNumberString(2)
    
    def AMT_LAST_PYMT_RCVD(self):
        return randomNumberString(2)
    
    def CHARGE_OFF_DATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def CHARGE_OFF_AMT(self):
        return randomNumberString(2)
    
    def WRITE_OFF_DATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def AMOUNT_IN_DEFAULT(self):
        return randomNumberString(2)
    
    def WRITE_OFF_AMOUNT(self):
        return randomNumberString(2)
    
    def CUSTOMER_BIRTH_DATE(self, fake_var):
        return random_date(fake_var, '0111950', '28022020')
    
    def ORIGINAL_PRODUCT(self):
        return ''
    
    def ORIGINAL_PRODUCT_DESC(self):
        return ''
    
    def PRIMARY_OFFICER(self):
        return ''
    
    def CUSTOMER_TYPE(self):
        return ''
    
    def CURRENT_LIMIT_AMOUNT(self):
        return randomNumberString(3)
    
    def FEES_YET_TO_CHARGE(self):
        return ''
    
    def ACCRUED_INT_TO_DATE(self, fake_var):
        return random_date(fake_var, '01122019', '28022020')
    
    def SCHEDULED_MNTHLY_REPAY_AMT(self):
        return randomNumberString(2)
    
    def APR_RATE(self):
        return randomNumberString(1)
    
    def SCHEDULED_DT_OF_NEXT_PYMT(self):
        return '' 
    
    def MOBILE(self, fake_var):
        mobile = fake_var.cellphone_number()
        mobile = ''.join(char for char in mobile if char.isalnum())
        return mobile
    
    def OFFICE_PHONE_NUMBER(self, fake_var):
        phone_number = fake_var.phone_number()
        phone_number = ''.join(char for char in phone_number if char.isdigit())
        return phone_number
    
    def BUSINESS_PHONE_NUMBER(self, fake_var):
        phone_number = fake_var.phone_number()
        phone_number = ''.join(char for char in phone_number if char.isdigit())
        return phone_number
    
    def HOME_PHONE_NUMBER(self, fake_var):
        phone_number = fake_var.phone_number()
        phone_number = ''.join(char for char in phone_number if char.isdigit())
        return phone_number
    
    def PERSONAL_EMAIL_ADRESS(self, fake_var):
        return fake_var.email()
    
    def BUSINESS_EMAIL_ADDRESS(self, fake_var):
        return fake_var.email()
    
    def PREFERRED_CONTACT_NUMBER(self, fake_var):
        phone_number = fake_var.phone_number()
        phone_number = ''.join(char for char in phone_number if char.isdigit())
        return phone_number
    
    def PREFERRED_METHOD_OF_CONTACT(self, fake_var):
        return random.choice(['PHONE','FAX','POST','MOBILE'])
    
    def PREFERRED_TIME_OF_CONTACT(self):
        return random.choice(['MORNING','AFTERNOON','EVENING'])
    
    def ACCOUNT_NO(self, account_nos):
        return random.choice(account_nos)
    
    def ADDRESS1(self):
        return self.address[0]
    
    def ADDRESS2(self):
        return self.address[1]
    
    def STREET(self):
        return self.address[2]
    
    def POST_CODE(self):
        return self.address[-1]
    
    def CUSTOMER_TITLE(self):
        return self.name[0]
    
    def FIRST_NAME(self):
        return self.name[1]
    
    def LAST_NAME(self):
        return self.name[2]
    
    def CUSTOMER_NAME(self):
        return ' '.join([self.name[0],self.name[1],self.name[2]])

class SETL_STMT():
    def __init__(self, account_nos, fake_var, Counties, Industry_dict):
        self.name = fake_name(fake_var)
        self.address = fake_address(fake_var, Counties)
        self.industry = random_industry(Industry_dict)
        
        
        self.creationdate = SETL_STMT_fields.CREATIONDATE(self, fake_var)
        self.reportingreferencedate  = SETL_STMT_fields.REPORTINGREFERENCEDATE(self, fake_var)
        self.runid = SETL_STMT_fields.RUNID(self)
        self.rec_time = SETL_STMT_fields.REC_TIME(self, fake_var)
        self.product_description = SETL_STMT_fields.PRODUCT_DESCRIPTION(self)
        self.currency = SETL_STMT_fields.CURRENCY(self)
        self.account_status = SETL_STMT_fields.ACCOUNT_STATUS(self)
        self.exposure_in_default = SETL_STMT_fields.EXPOSURE_IN_DEFAULT(self)
        self.watchlist_indicator = SETL_STMT_fields.WATCHLIST_INDICATOR(self)
        self.account = SETL_STMT_fields.ACCOUNT(self, account_nos)
        self.product = SETL_STMT_fields.PRODUCT(self)
        self.customer_id = SETL_STMT_fields.CUSTOMER_ID(self)
        self.posting_restriction = SETL_STMT_fields.POSTING_RESTRICTION(self)
        self.account_status_date = SETL_STMT_fields.ACCOUNT_STATUS_DATE(self, fake_var)
        self.account_balance = SETL_STMT_fields.ACCOUNT_BALANCE(self)
        self.current_month_due_amount = SETL_STMT_fields.CURRENT_MONTH_DUE_AMOUNT(self)
        self.overdue_amount = SETL_STMT_fields.OVERDUE_AMOUNT(self)
        self.total_due_amount = SETL_STMT_fields.TOTAL_DUE_AMOUNT(self)
        self.capital = SETL_STMT_fields.CAPITAL(self)
        self.fees = SETL_STMT_fields.FEES(self)
        self.interest = SETL_STMT_fields.INTEREST(self)
        self.days_overdue = SETL_STMT_fields.DAYS_OVERDUE(self)
        self.date_last_pymt_rcvd = SETL_STMT_fields.DATE_LAST_PYMT_RCVD(self)
        self.amt_last_pymt_rcvd = SETL_STMT_fields.AMT_LAST_PYMT_RCVD(self)
        self.charge_off_date = SETL_STMT_fields.CHARGE_OFF_DATE(self, fake_var)
        self.charge_off_amt = SETL_STMT_fields.CHARGE_OFF_AMT(self)
        self.write_off_date = SETL_STMT_fields.WRITE_OFF_DATE(self, fake_var)
        self.amount_in_default = SETL_STMT_fields.AMOUNT_IN_DEFAULT(self)
        self.write_off_amount = SETL_STMT_fields.WRITE_OFF_AMOUNT(self)
        self.customer_birth_date = SETL_STMT_fields.CUSTOMER_BIRTH_DATE(self, fake_var)
        self.original_product = SETL_STMT_fields.ORIGINAL_PRODUCT(self)
        self.original_product_desc = SETL_STMT_fields.ORIGINAL_PRODUCT_DESC(self)
        self.primary_officer = SETL_STMT_fields.PRIMARY_OFFICER(self)
        self.customer_type = SETL_STMT_fields.CUSTOMER_TYPE(self)
        self.current_limit_amount = SETL_STMT_fields.CURRENT_LIMIT_AMOUNT(self)
        self.fees_yet_to_charge = SETL_STMT_fields.FEES_YET_TO_CHARGE(self)
        self.accrued_int_to_date = SETL_STMT_fields.ACCRUED_INT_TO_DATE(self, fake_var)
        self.scheduled_mnthly_repay_amt = SETL_STMT_fields.SCHEDULED_MNTHLY_REPAY_AMT(self)
        self.apr_rate = SETL_STMT_fields.APR_RATE(self)
        self.scheduled_dt_of_next_pymt = SETL_STMT_fields.SCHEDULED_DT_OF_NEXT_PYMT(self)
        self.mobile = SETL_STMT_fields.MOBILE(self, fake_var)
        self.office_phone_number = SETL_STMT_fields.OFFICE_PHONE_NUMBER(self, fake_var)
        self.business_phone_number = SETL_STMT_fields.BUSINESS_PHONE_NUMBER(self, fake_var)
        self.home_phone_number = SETL_STMT_fields.HOME_PHONE_NUMBER(self, fake_var)
        self.personal_email_adress = SETL_STMT_fields.PERSONAL_EMAIL_ADRESS(self, fake_var)
        self.business_email_address = SETL_STMT_fields.BUSINESS_EMAIL_ADDRESS(self, fake_var)
        self.preferred_contact_number = SETL_STMT_fields.PREFERRED_CONTACT_NUMBER(self, fake_var)
        self.preferred_method_of_contact = SETL_STMT_fields.PREFERRED_METHOD_OF_CONTACT(self, fake_var)
        self.preferred_time_of_contact = SETL_STMT_fields.PREFERRED_TIME_OF_CONTACT(self)
        self.account_no = SETL_STMT_fields.ACCOUNT_NO(self, account_nos)
        self.address1 = SETL_STMT_fields.ADDRESS1(self)
        self.address2 = SETL_STMT_fields.ADDRESS2(self)
        self.street = SETL_STMT_fields.STREET(self)
        self.post_code = SETL_STMT_fields.POST_CODE(self)
        self.customer_title = SETL_STMT_fields.CUSTOMER_TITLE(self)
        self.first_name = SETL_STMT_fields.FIRST_NAME(self)
        self.last_name = SETL_STMT_fields.LAST_NAME(self)
        self.customer_name = SETL_STMT_fields.CUSTOMER_NAME(self)


        
ACCOUNT_NOS = get_account_nos()
FAKE = Faker('en_GB')
LOOP_COUNTER = 1000
UK_COUNTIES = get_UK_Counties()
INDUSTRIES = get_industries()
with open('Account_arrears_history.csv', 'w', newline='') as csvfile:
    combination_wrtier = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in range(LOOP_COUNTER):
        if i == 0:
            headers = SETL_STMT_headers()
            combination_wrtier.writerow(headers.setl_stmt_headers)
            
        else:
            SETL_customer = SETL_STMT(ACCOUNT_NOS, FAKE, UK_COUNTIES, INDUSTRIES)
            SETL_info = [SETL_customer.creationdate,
                        SETL_customer.reportingreferencedate,
                        SETL_customer.runid,
                        SETL_customer.rec_time,
                        SETL_customer.product_description,
                        SETL_customer.currency,
                        SETL_customer.account_status,
                        SETL_customer.exposure_in_default,
                        SETL_customer.watchlist_indicator,
                        SETL_customer.account,
                        SETL_customer.product,
                        SETL_customer.customer_id,
                        SETL_customer.posting_restriction,
                        SETL_customer.account_status_date,
                        SETL_customer.account_balance,
                        SETL_customer.current_month_due_amount,
                        SETL_customer.overdue_amount,
                        SETL_customer.total_due_amount,
                        SETL_customer.capital,
                        SETL_customer.fees,
                        SETL_customer.interest,
                        SETL_customer.days_overdue,
                        SETL_customer.date_last_pymt_rcvd,
                        SETL_customer.amt_last_pymt_rcvd,
                        SETL_customer.charge_off_date,
                        SETL_customer.charge_off_amt,
                        SETL_customer.write_off_date,
                        SETL_customer.amount_in_default,
                        SETL_customer.write_off_amount,
                        SETL_customer.customer_birth_date,
                        SETL_customer.original_product,
                        SETL_customer.original_product_desc,
                        SETL_customer.primary_officer,
                        SETL_customer.customer_type,
                        SETL_customer.current_limit_amount,
                        SETL_customer.fees_yet_to_charge,
                        SETL_customer.accrued_int_to_date,
                        SETL_customer.scheduled_mnthly_repay_amt,
                        SETL_customer.apr_rate,
                        SETL_customer.scheduled_dt_of_next_pymt,
                        SETL_customer.mobile,
                        SETL_customer.office_phone_number,
                        SETL_customer.business_phone_number,
                        SETL_customer.home_phone_number,
                        SETL_customer.personal_email_adress,
                        SETL_customer.business_email_address,
                        SETL_customer.preferred_contact_number,
                        SETL_customer.preferred_method_of_contact,
                        SETL_customer.preferred_time_of_contact,
                        SETL_customer.account_no,
                        SETL_customer.address1,
                        SETL_customer.address2,
                        SETL_customer.street,
                        SETL_customer.post_code,
                        SETL_customer.customer_title,
                        SETL_customer.first_name,
                        SETL_customer.last_name,
                        SETL_customer.customer_name,
]
            combination_wrtier.writerow(SETL_info)
