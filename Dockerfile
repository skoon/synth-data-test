# 1.Base our docker image on alpine. Alpine is lightweight operating system good for creating dockerised apps
# 2.when we build our image we can access any scripts when running on container
# 3.copy requirements file to docker image. used to install relavent packages needed for app
# 4.install packages needed to install uwsg server. apk-alpinepackage, add-install new package, update-package repo updated when run command
# nocache-dont want to cache anything to keep lightweight, virtual .tmp
# 5. pip install - install dependencies
# 6. run apk - remove virtual requirements

FROM python:3.8-alpine   
ENV PATH="/scripts:${PATH}"
COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN pip install -r /requirements.txt
RUN apk del .tmp

# make new directory in dockerfile
# copy SyntheticData from root file into app (into dockerfile)
# change into app directory 
# copy scripts folder into dockerfile
RUN mkdir /app
COPY ./SyntheticData /app
WORKDIR /app
COPY ./scripts /scripts

# add excecutable permissions to scripts
RUN chmod +x /scripts/*

# create 2 new directories in our docker image. -p means create all subdirectories aswell
RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/
# /vol/web/static ^?

RUN adduser -D user
RUN chown -R user:user /vol
RUN chmod -R 755 /vol/web
USER user

CMD ["entrypoint.sh"]


# ENV PYTHONUNBUFFERED=1
# WORKDIR /code
# COPY . /code/