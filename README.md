# Synthetic Data Tool
This Synthetic Data tool allows the user to generate datasets filled with random test data of a specific format. This allows for realistic test data to be generated on mass, quickly. This may be particularly useful in Performance Testing and Database Testing.

## 1.1 Downloading the file
You can either clone the repo using git e.g. `git clone https://skoon@bitbucket.org/deloittetesting/synthetic-data-tool.git`

Or you can navigate to the Downloads section on the side menu in bitbucket to dowload the zip file for this repo.

## 1.2 Prerequisites
The below is required to run the tool locally:

1. Python 3 (packages needed: pip) 
2. Docker (with docker compose)

## 1.3 Running Locally

In the command terminal:

1 - Navigate to Synthetic_Data_Tool (root directory) `cd .\Synthetic_Data_Tool\`

2 - Run docker compose to start up app in container  `docker-compose up`

3 - Open application on browser. URL specified = http://127.0.0.1:8000/







